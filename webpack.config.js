const webpack = require('webpack');

function getEntrySources(sources) {
  if (process.env.NODE_ENV !== 'production') {
    sources.push('webpack-dev-server/client?http://localhost:8080');
    sources.push('webpack/hot/only-dev-server');
  }

  return sources;
}

module.exports = {
  entry: {
    bundle: getEntrySources([
      './src/index.js',
    ]),
  },
  output: {
    publicPath: 'http://localhost:8080/',
    filename: 'dist/[name].js',
  },
  plugins: [
    new webpack.DefinePlugin({
      __DEV__: process.env.NODE_ENV !== 'production',
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
    }),
  ],
  module: {
    loaders: [{
      test: /\.js$/,
      loader: 'babel!eslint-loader',
      exclude: /node_modules/,
    }],
  },
};
