import { combineReducers } from 'redux'
import input from './input'

const rootReducer = combineReducers({
  input
})

export default rootReducer
