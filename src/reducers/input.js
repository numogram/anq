const initialState = {};

const input = (state = initialState, action) => {
  switch (action.type) {
    case 'MUTATE_INPUT':
      return {
        text: action.text
      }
    default:
      return state
  }
}

export default input;
