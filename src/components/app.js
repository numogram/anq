import React from 'react';
import Input from './input';
import Output from './output';

const App = () => (
  <div>
    <Input />
    <Output />
  </div>
);

export default App;
