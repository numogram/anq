import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

function mapStateToProps(state) {
  return {
    input: state.input.text
  }
}

class Output extends Component {
  render() {
    const AQ = {
      '0': 0,
      '1': 1,
      '2': 2,
      '3': 3,
      '4': 4,
      '5': 5,
      '6': 6,
      '7': 7,
      '8': 8,
      '9': 9,
      'a': 10,
      'b': 11,
      'c': 12,
      'd': 13,
      'e': 14,
      'f': 15,
      'g': 16,
      'h': 17,
      'i': 18,
      'j': 19,
      'k': 20,
      'l': 21,
      'm': 22,
      'n': 23,
      'o': 24,
      'p': 25,
      'q': 26,
      'r': 27,
      's': 28,
      't': 29,
      'u': 30,
      'v': 31,
      'w': 32,
      'x': 33,
      'y': 34,
      'z': 35
    }

    function parse(kmer, type = AQ) {
      let mapped = validate(kmer).split('')
        .map((mer) => type[mer])
      if (mapped.length > 0) {
        return mapped.reduce((a, b) => a + b);
      }
      else {
        return 0;
      }
    }

    function validate(kmer = '') {
      return kmer.toLowerCase().replace(/[^a-z0-1]/g, '');
    }

    const input = parse(this.props.input);
    return (
      <div>
        { input }
      </div>
    );
  }
}

export default connect(
  mapStateToProps
)(Output);
