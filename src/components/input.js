import React from 'react'
import { connect } from 'react-redux'
import { mutateInput } from '../actions'

let Input = ({ dispatch }) => {
  let input

  return (
    <div>
      <form onSubmit={e => {
        e.preventDefault()
        if (!input.value.trim()) {
          return
        }
        dispatch(mutateInput(input.value))
      }}>
        <input ref={node => {
          input = node
        }} />
        <button type="submit">
          Query
        </button>
      </form>
    </div>
  )
}
Input = connect()(Input)

export default Input
