export const mutateInput = (text) => {
  return {
    type: 'MUTATE_INPUT',
    text
  }
}
